var assert = require('chai').assert;
var serialization = require('../src/serialization');

var sigs = require('./signatures');
var signatures = sigs.signatures;
var signers = sigs.signers;

describe('Encode-decode tests', () => {

    function test(object) {
        var buffer = serialization.encode(object);
        console.log(`Encoded buffer: ${buffer.toString('hex')}`);
        assert.deepEqual(serialization.decode(buffer), object);
    }

    it('empty call should be ok', () => {
        test({calls: [], signers: [], signatures: []});
    })

    it('empty call with one signature', () => {
        test({calls: [], signers: [], signatures: signatures(1)});
    })

    it('single call with no param', () => {
        test({
            calls: [{functionName: 'func1åäö', args: []}],
            signers: [],
            signatures: signatures(1)
        });
    })

    it('single call with no signatures ok', () => {
        // This is only used when signing and validating signatures.
        // signatures must not be present in the signed data.
        test({calls: [{functionName: 'func1åäö', args: []}], signers: signers(2)});
    })

    function testSingleArg(arg) {
        test({calls: [{functionName: 'f', args: [arg]}], signers: [], signatures: []});
    }

    it('single call with single integer param', () => {
        testSingleArg(1);
    })

    it('single call with single negative integer param', () => {
        testSingleArg(-1);
    })

    function testSingleArgAssertFail(value) {
        try {
            testSingleArg(value);
        } catch (error) {
            console.log(error);
            assert.ok(error instanceof Error);
            return;
        }
        assert.fail();
    }


    it('minimum integer param', () => {
        testSingleArg(-Math.pow(2, 47));
    })

    it('too low integer param', () => {
        testSingleArgAssertFail(-Math.pow(2, 47) - 1);
    })

    it('maximum integer param', () => {
        testSingleArg(Math.pow(2, 47) - 1);
    })

    it('too high integer param', () => {
        testSingleArgAssertFail(Math.pow(2, 47));
    })

    it('single call with single maximum integer param', () => {
        testSingleArg(0);
    })

    it('single call with single string param', () => {
        testSingleArg('hello');
    })

    it('single call with single buffer param', () => {
        testSingleArg(Buffer.from('hello', 'utf8'));
    })

    it('single call with single array param', () => {
        testSingleArg(['a']);
    })

    it('single call with multiple mixed type param', () => {
        test({
            calls: [{
                functionName: 'func1åäö',
                args: ['abc åäö€', 1, -1,
                    ['', 1, 0, ['a', 'b'], Buffer.from('hi', 'utf8')],
                    Buffer.from('hello', 'utf8'), [], Buffer.alloc(0)]
            }], signers: [],
            signatures: []
        });
    })

    it('multiple calls with multiple mixed type param', () => {
        test({
            calls: [{functionName: 'func0', args: []},
                {functionName: 'func1', args: [1, 'a', ['c', 'd'], [1], Buffer.alloc(2, 3)]},
                {
                    functionName: 'func3åäö',
                    args: ['abc åäö€', 1, -1,
                        ['', 1, 0, ['a', 'b'], Buffer.from('hi', 'utf8')],
                        Buffer.from('hello', 'utf8'), [], Buffer.alloc(0)]
                }], signers: [],
            signatures: []
        });
    })

    it('multiple calls with multiple signatures', () => {
        test({
            calls: [{functionName: 'func1åäö', args: ['abc åäö€', 1, -1]}],
            signers: signers(3),
            signatures: signatures(4)
        });
    });
});
